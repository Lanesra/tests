var page = require('webpage').create();
var url = 'http://doska.ykt.ru/post-add';
page.onConsoleMessage = function(msg) {
  console.log(msg);
};
page.open(url, function (status) {
  page.evaluate(function() {
    var changeEvent = new Event('change'),
        cats = document.getElementById('cats'),
        subcats = document.getElementById('subcats'),
        rubrics = document.getElementById('rubrics'),
        queue = [cats, subcats, rubrics],
        i=0;

    function setField(obj) {
      obj.options[1].selected = true;
      obj.dispatchEvent(changeEvent);
      next();
    }

    function next(){
      setTimeout(function() {
        queue[i]  &&  setField(queue[i++]);
      }, 10);
    }
    next();
  });
  page.evaluate(function() {
  var $fields = $('#newpost div.d-post-form_group');

  for (var i=0; i<$fields.length; i++) {
    var $field = $fields.eq(i);
    var $label = $field.find('.d-post-form_label');
    if ($field.hasClass('required')) {
      console.log('Поле',i , 'обязательно');
      //console.log('Поле "',$label.html(), '" обязательно');
    }
    //else console.log('Поле "',$label.html(), '" не обязательно');
    else console.log('Поле',i , 'не обязательно');
  }


  //  var $categoryDropdown = $('#cats.d-post-form_category.yui-input-control');
  //  console.log('Категория обязательна', $categoryDropdown.attr('required') == 'required');
});

  setTimeout(function () {
    phantom.exit();
  }, 500);
});
