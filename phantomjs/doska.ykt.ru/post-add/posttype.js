var page = require('webpage').create();
var url = 'http://doska.ykt.ru/post-add';
page.onConsoleMessage = function(msg) {
  console.log(msg);
};

page.open(url, function (status) {
  page.evaluate(function() {
    function setPostType(time, i) {
      setTimeout(function () {
        var clickEvent = new Event('click'),
            servicePack = document.getElementsByName('service-pack'),
            $tabs = $('div.d-sp_tab'),
            $typeTab = $tabs.eq(i);
        servicePack[i].checked = true;
        document.getElementsByClassName('d-sp_tab_inner')[i].dispatchEvent(clickEvent);
        setTimeout(function () {
          console.log('\nВыбрано поле', $("input[name='service-pack']:checked").val(), $typeTab.hasClass('active'));
        }, 100);
      }, time);
    }

    function showContentBody(time, i){
      setTimeout(function () {
        var $panels = $('div.d-sp_panel'),
            $typeContent = $panels.eq(i),//.d-sp_item.d-sp_item--vip');
            $typeContentBody = $typeContent.find('.d-sp_body'),
            $typeItems = $typeContentBody.find('div.d-sp_item');
        for (var j=0; j<$typeItems.length; j++) {
          var $field = $typeItems.eq(j);
          if ($field.css('display') !== 'none') {
            var $desc = $field.find('.d-sp_item_desc');
            console.log($desc.find('.d-sp_item_title').html());
          }
        }
        console.log($('.d-sp_price').html());
      }, time);
    }
    setPostType(100, 0);
    showContentBody(300, 0);
    setPostType(300, 1);
    showContentBody(500, 1);
    setPostType(500, 2);
    showContentBody(700, 2);
  });

  setTimeout(function () {
    page.render('posttype.png');
    phantom.exit();
  }, 1500);
});
