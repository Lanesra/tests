var page = require('webpage').create();
var url = 'http://ykt.ru/';

page.onConsoleMessage = function(msg) {
  console.log(msg);
};

page.open(url, function (status) {

  function wrapperTesting() {
    page.evaluate(function() {
      if ($('#ygm-wrapper').children().length != 4){
        console.error('Выведены не все поля');
      }
    });
  }

  function linkTesting(time) {
    page.evaluateAsync(function() {
      var $ygmLinks = $('#ygm-links > li');
      for (var i=0; i<$ygmLinks.length-1; i++) {
        try {
          console.log(' -', $ygmLinks.eq(i).find('a').attr('title'));
        }
        catch (e) {
          console.error('Title is undefined');
        }
      }
      if ($ygmLinks.eq(i).attr('id') == 'ygm-link-more-link') {
        console.log('Выведены все ссылки\n');
      } else {
        console.error('Выведены не все ссылки\n');
      }
      var $moreLinks = $('.no-select.bg-blue-hover');
      console.log('Поле "Еще" скрыто:', !$moreLinks.hasClass('active'));

      if (($ygmLinks.length + $moreLinks.find('.unstyled > li').length) != 21){
        console.error('Выведены не все ссылки\n');
      }
      $moreLinks.trigger('click');
      setTimeout(function () {
        console.log('Поле "Еще" появилось:', $moreLinks.hasClass('active'));
      }, 100);
    }, time);
  }

  function weatherTesting(time) {
    page.evaluateAsync(function() {
      var $ygmWeather = $('#ygm-weather-preview');
      console.log('Поле "Погода" скрыто:', !$('#ygm-weather').hasClass('active'));
      $ygmWeather.trigger('click');
      setTimeout(function () {
        console.log('Поле "Погода" появилось:', $('#ygm-weather').hasClass('active'));
      }, 100);
    }, time);
  }

  wrapperTesting();
  linkTesting(100);
  weatherTesting(300);
  
  setTimeout(function () {
    page.render('main.png');
    phantom.exit();
  }, 1000);
});
