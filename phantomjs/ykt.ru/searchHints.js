var page = require('webpage').create();
var url = 'http://ykt.ru/';
page.onConsoleMessage = function(msg) {
  console.log(msg);
};

page.open(url, function (status) {
  page.evaluate(function() {
    var words = ['дом', 'home', 'этиҥ', '2015'];
    function inputData(word){
      $('#q-input').val(word);
      var e = new Event('keydown');
      document.getElementById('q-input').dispatchEvent(e);
    }

    function outputResults(){
      console.log($('#ui-id-1').css('display'));
    }

    for (var i=0; i<4; i++){
      inputData(words[i]);
      setTimeout(function() {
        outputResults();
      });
    }
  });
  setTimeout(function () {
    page.render('search.png');
    phantom.exit();
  }, 100);
});
/*//для консоли
var words = ['дом', 'home', 'этиҥ', '2015'];
var i = 0;
$('#q-input').val(words[i]);
var e = new Event('keydown');
document.getElementById('q-input').dispatchEvent(e);
//$('#ui-id-1').css('display')
*/
