var assert = require('assert'),
test = require('selenium-webdriver/testing'),
webdriver = require('selenium-webdriver');
var driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();
test.describe('Input key', function() {
    var keys = ["дом", "home", "этиҥ", "2015"];
    keys.forEach(function(item, i, keys){
      test.it('should work', function() {
          driver.get('http://ykt.ru/search/');
          var searchBox = driver.findElement(webdriver.By.name('q'));
          searchBox.then(function(value){
            value.sendKeys(item);
            value.getAttribute('value').then(function(value) {
                assert.equal(value, item);
            });
          })
          if (i==3) driver.quit();
        });
    });
});
