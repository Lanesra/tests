"use strict";

var webdriver = require('selenium-webdriver');
var browser = new webdriver.Builder().usingServer().withCapabilities({'browserName': 'chrome' }).build();
var i=0, count=0;
function clickLink(link) {
	link.click();
	return link;
}

function getClassName(link) {
	return link.getAttribute('class');
}

function getText(link) {
	return link.getText();
}

function findItem() {
	return browser.findElements(webdriver.By.className('yui-nav_item')).then(function(result) {
		return result[i];
	});
}

function closeBrowser() {
	browser.quit();
}

function handleFailure(err) {
	console.error('Something went wrong\n', err.stack, '\n');
	closeBrowser();
}

browser.get('http://ykt.ru/');
var titles = ['Все','Новости','Интересно','Скидки','Sale'];
while (count!=5){
	browser.wait(findItem, 1000).then(clickLink).then(getClassName).then(function(val) {
		console.log('Выбрана ссылка', titles[i++], val=='yui-nav_item active');
	});
	count++;
}
closeBrowser();
