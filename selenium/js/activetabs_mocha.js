"use strict";

var assert = require('assert'),
test = require('selenium-webdriver/testing'),
webdriver = require('selenium-webdriver');
var browser = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();
var i=0, count=0;
function clickLink(link) {
	link.click();
	return link;
}

function getClassName(link) {
	return link.getAttribute('class');
}

function findItem() {
	return browser.findElements(webdriver.By.className('yui-nav_item')).then(function(result) {
		return result[i++];
	});
}

test.describe('Click news tabs', function() {
	browser.get('http://ykt.ru/');
	while (count!=5){
		var value = browser.wait(findItem, 1000).then(clickLink).then(getClassName);
		test.it('Tab is active', function() {
			value.then(function(val){
				assert.equal(val,'yui-nav_item active');
			});
		 });
		count++;
		if (count ==5)
				browser.quit();
	}
});
