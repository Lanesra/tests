"use strict";

var webdriver = require('selenium-webdriver');
var browser = new webdriver.Builder().usingServer().withCapabilities({'browserName': 'chrome' }).build();
var i=0;

function clickLink(link) {
	link.click();
}

function handleFailure(err) {
	console.error('Something went wrong\n', err.stack, '\n');
	closeBrowser();
}

function findItem() {
	return browser.findElements(webdriver.By.className('lettersli')).then(function(result) {
		return result[i++];
	});
}

function closeBrowser() {
	browser.quit();
}

browser.get('http://ykt.ru/');
var count=0;
while (count!=5){
	browser.wait(findItem, 100).then(clickLink);
	count++;
}
var input = browser.findElement(webdriver.By.id('q-input'));
var promise = input.getAttribute('value');
promise.then(function(value) {
	console.log('Выведены все буквы:', value == 'һөҕүҥ');
}).then(closeBrowser, handleFailure);
