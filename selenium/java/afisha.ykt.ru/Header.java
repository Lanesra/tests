package afisha_ykt_ru;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.List;

public class Header {
    static WebDriver driver = new ChromeDriver();
    static WebDriverWait wait = new WebDriverWait(driver, 10);

    @Before
    public void start() {
        driver.get("http://www.afisha.ykt.ru/");
    }

    @Test
    public void testHeader() throws Exception{
        for (int i=0; i<9; i++){
            if (i>6){
                clickMore();
                clickItem(i);
            }
            else {
                clickItem(i);
                isActive(i);
                equalDates();
            }
        }
    }

    private static void clickItem(int i){
        List<WebElement> items = driver.findElements(By.className("yui-nav_item"));
        WebElement item = wait.until(ExpectedConditions.elementToBeClickable(items.get(i)));
        item.click();
    }

    private static void clickMore(){
        WebElement more = wait.until(ExpectedConditions.elementToBeClickable(By.className("a-nav_more")));
        more.click();
    }

    private static void isActive(int i){
        List<WebElement> items = driver.findElements(By.className("yui-nav_item"));
        WebElement item = wait.until(ExpectedConditions.elementToBeClickable(items.get(i)));
        System.out.print("Открыта ссылка ");
        System.out.println(item.getAttribute("class").equals("yui-nav_item active"));
    }

    private static String activeTimeItem(){
        WebElement wrapper = driver.findElement(By.cssSelector(".yui-nav.tab.afisha.wrapper.timeline")),
                item = wrapper.findElement(By.cssSelector(".yui-nav_item.active"));
        return item.getText();
    }

    private static String nearestEvent(){
        WebElement event = driver.findElement(By.className("category_events_date"));
        return event.getText();
    }

    private static void equalDates(){
        if (activeTimeItem().equals(nearestEvent()))
            System.out.println(" Даты совпадают ");
        else System.out.println(" Даты не совпадают " + activeTimeItem() +" "+ nearestEvent());
    }

    private static void makeScreenshot() throws Exception {
        File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screen, new File("screen.png"));
    }

    @After
    public void finish() throws Exception{
        makeScreenshot();
        driver.quit();
    }
}