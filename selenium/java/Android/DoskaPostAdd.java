import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;

public class DoskaPostAdd {
    private static WebDriver driver;
    private static WebDriverWait wait;

    @BeforeClass
    public static void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities=DesiredCapabilities.android();

        capabilities.setCapability("noReset", "true");
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, BrowserType.CHROME);
        capabilities.setCapability(MobileCapabilityType.PLATFORM, Platform.ANDROID);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,"Android Emulator");
        capabilities.setCapability(MobileCapabilityType.VERSION,"6.0");

        URL url= new URL("http://127.0.0.1:4723/wd/hub");
        driver = new AndroidDriver(url, capabilities);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("http://doska.ykt.ru/post-add");
        wait = new WebDriverWait(driver, 10);
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

    private void makeScreenshot() throws Exception {
        File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screen, new File("screen.png"));
    }

    //Недвижимость-Квартиры-Продаю
    @Test
    public void dropdown() throws Exception {
        WebElement cats = driver.findElement(By.id("cats"));
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", cats);
        Select catsFields = new Select(cats);
        catsFields.selectByValue("139");

        List<WebElement> subcats = driver.findElement(By.id("subcats")).findElements(By.cssSelector("option"));
        Assert.assertEquals("Дропдаун подкатегории определен неверно ",8, subcats.size());
        Select subcatsFields = new Select(driver.findElement(By.id("subcats")));
        subcatsFields.selectByValue("270");

        List<WebElement> rubrics = driver.findElement(By.id("rubrics")).findElements(By.cssSelector("option"));
        assertEquals("Дропдаун рубрики определен неверно ", 6, rubrics.size());
        Select rubricsFields = new Select(driver.findElement(By.id("rubrics")));
        rubricsFields.selectByValue("326");

        makeScreenshot();
    }

    @Test
    public void requieredFields() {
        List<WebElement> fields = driver.findElements(By.className("d-input_label"));
        for (WebElement f : fields) {
            System.out.print(f.getText());
            try {
                String s = f.findElement(By.cssSelector("span")).getAttribute("style");
                System.out.println("Обязательно");
            }
            catch (NoSuchElementException e) {
                System.out.println("Не обязательно");
                continue;
            }
        }
    }
}
