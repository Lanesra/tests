package doska_ykt_ru;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;

public class FloatingBanners {
    WebDriver driver = new ChromeDriver();
    WebDriverWait wait = new WebDriverWait(driver, 10);

    @Before
    public void start() throws Exception{
        driver.get("http://doska.ykt.ru/");
    }

    @Test
    public void TestMethod() throws Exception{
        JavascriptExecutor jse = ((JavascriptExecutor) driver);
        Long height = (Long) jse.executeScript("return document.body.scrollHeight");
        Integer j=0;
        String s="";
        for (Long i = j.longValue();i < height; i+=50) {
            jse.executeScript("window.scrollTo(0, "+i+")");
            WebElement banners = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("floatingBanners")));
            if (!s.equals(banners.getAttribute("style")) || s.equals("")){
                System.out.println(banners.getAttribute("style")+" "+i);
            }
            s =  banners.getAttribute("style");
        }
    }

    @After
    public void finish() throws Exception {
        File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screen, new File("screen.png"));
        driver.quit();
    }
}