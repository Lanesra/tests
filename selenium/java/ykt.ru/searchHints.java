import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class SearchHints {
    WebDriver driver;

    @Before
    public void start() throws Exception {
        driver = new ChromeDriver();
        driver.get("http://www.ykt.ru/");
    }

    @Test
    public void testMethod() throws Exception {
        String[] keys = {"дом", "home", "этиҥ", "2015"};
        for (String s : keys) {
            sendKey(s);
        }
    }

    private void sendKey(String word){
        WebElement query = driver.findElement(By.name("q"));
        query.clear();
        query.sendKeys(word);
        long end = System.currentTimeMillis() + 5000;
        while (System.currentTimeMillis() < end) {
            ArrayList<WebElement> resultsDiv = (ArrayList<WebElement>) driver.findElements(By.className("se_autocomplete--iconless"));
            if (resultsDiv.size()>0) {
                break;
            }
        }
        List<WebElement> allSuggestions = driver.findElements(By.xpath("//div[@class='se_autocomplete--iconless']"));
        for (WebElement suggestion : allSuggestions) {
            System.out.println(suggestion.getText());
        }
    }

    @After
    public void finish() throws Exception{
        File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screen, new File("screen.png"));
        driver.quit();
    }
}